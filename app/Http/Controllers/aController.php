<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class aController extends Controller
{
    public function export()
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $phpWord->addParagraphStyle('Heading2', array('alignment' => 'center'));
        $section = $phpWord->addSection();
        $html = view('export');
        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html, false, false);
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('word.docx');
        return response()->download(('word.docx'));
    }

    public function view()
    {
        return view('a');
    }
}
